This repository contains all scripts that are used to create:
- Teams (incl. Characters) for the website, ysoccer and the upcoming tabletop
- Namelists

## Installation
- `git clone https://gitlab.com/eondra/ueff/ueff-generator`
- `cd ueff-generator`
- `./install.sh`

## How it works
After the installation, you are able to run the ueff-generator menu with `ueff`
