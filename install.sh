#!/bin/bash

UEFF_FOLDER="${HOME}/.ueff"
CONFIG_FOLDER="${HOME}/.config/ueff/config"
SCRIPT_FOLDER="/usr/lib/ueff/scripts"

function getuserinput() {
    echo "Welcome to the UEFF installer."
    read -p "Do you want to continue the installation? (y/N)" INSTALL_YN

    if [ "$INSTALL_YN" == Y ] || [ "$INSTALL_YN" == y ];then
        echo "Installation confirmed"
    else
        echo "Cancel installation"
        exit 0
    fi
}

function movefiles() {
    echo "Installing..."
    mkdir -p $UEFF_FOLDER $CONFIG_FOLDER
    sudo mkdir -p $SCRIPT_FOLDER
    cp -r config/* $CONFIG_FOLDER
    sudo cp -r scripts/* $SCRIPT_FOLDER
    sudo cp ueff /usr/bin/
    echo "Installation complete"
}

function main() {
    getuserinput
    movefiles
}

main
