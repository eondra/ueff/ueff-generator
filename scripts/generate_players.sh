#!/bin/bash

source ${CONFIG_FOLDER}/general.conf

function rollRace() {
    # Roll characters race
    ROLL_RACE_CHANCE=$(shuf -i 1-100 -n 1)
    if (( $ROLL_RACE_CHANCE <= 25 ))
    then
        CHARACTER_RACE_INDEX=$(shuf -i 1-${#RACE_ARRAY[@]} -n 1)-1
        source ${CONFIG_FOLDER}/races/${RACE_ARRAY[$CHARACTER_RACE_INDEX]}.conf
        CHARACTER_RACE=${RACE_ARRAY[$CHARACTER_RACE_INDEX]}
        CHARACTER_RACE_MD=${RACE_NAME}
    else
        source ${CONFIG_FOLDER}/nations/${TEAM_NATION_LC}.conf
        CHARACTER_RACE=${NATION_PRIM_RACE}
        CHARACTER_RACE_MD=${NATION_PRIM_RACE_MD}
    fi
}

function rollNationality() {
    # Roll characters nationality
    ROLL_NATIONALITY_CHANCE=$(shuf -i 1-100 -n 1)

    if (( $ROLL_NATIONALITY_CHANCE <= 25 ))
    then
        CHARACTER_NATIONALITY_INDEX=$(shuf -i 1-${#NATIONS_ARRAY[@]} -n 1)-1
        source ${CONFIG_FOLDER}/nations/${NATIONS_ARRAY[$CHARACTER_NATIONALITY_INDEX]}.conf
        CHARACTER_NATIONALITY=${NATION_FULL_NAME_LOWERC}
        CHARACTER_NATIONALITY_MD=${NATION_FULL_NAME_MD}
        CHARACTER_NATIONALITY_INGAME=${NATION_CODE_INGAME}
    else
        source ${CONFIG_FOLDER}/nations/${TEAM_NATION_LC}.conf
        CHARACTER_NATIONALITY=${NATION_FULL_NAME_LOWERC}
        CHARACTER_NATIONALITY_MD=${NATION_FULL_NAME_MD}
        CHARACTER_NATIONALITY_INGAME=${NATION_CODE_INGAME}
    fi

    # Roll characters birthcity
    source ${CONFIG_FOLDER}/nations/${CHARACTER_NATIONALITY}.conf
    ROLL_CITY_INDEX=$(shuf -i 1-${#CITIES_ARRAY[@]} -n 1)-1
    CHARACTER_BIRTH_CITY_MD=${CITIES_ARRAY[$ROLL_CITY_INDEX]}
    CHARACTER_BIRTH_CITY_WEB=$(echo ${CITIES_ARRAY[$ROLL_CITY_INDEX]} | tr '[:upper:]' '[:lower:]')
}

function rollName() {
    # Generate names
    readarray -t CHARACTER_GIVEN_NAME < ${CONFIG_FOLDER}/namelists/${CHARACTER_RACE}/name_given_${CHARACTER_RACE}.txt
    readarray -t CHARACTER_LAST_NAME < ${CONFIG_FOLDER}/namelists/${CHARACTER_RACE}/name_last_${CHARACTER_RACE}.txt

    ROLL_CHARACTER_GIVEN_NAME=$(shuf -i 1-${#CHARACTER_GIVEN_NAME[@]} -n 1)-1
    ROLL_CHARACTER_LAST_NAME=$(shuf -i 1-${#CHARACTER_LAST_NAME[@]} -n 1)-1

    CHARACTER_GIVEN_NAME=${CHARACTER_GIVEN_NAME[$ROLL_CHARACTER_GIVEN_NAME]}
    CHARACTER_LAST_NAME=${CHARACTER_LAST_NAME[$ROLL_CHARACTER_LAST_NAME]}

    # In-game names need to be uppercase
    CHARACTER_GIVEN_NAME_INGAME=$(echo ${CHARACTER_GIVEN_NAME[$ROLL_CHARACTER_GIVEN_NAME]} | tr [a-z] [A-Z])
    CHARACTER_LAST_NAME_INGAME=$(echo ${CHARACTER_LAST_NAME[$ROLL_CHARACTER_LAST_NAME]} | tr [a-z] [A-Z])

    # For the website we want the first character of the name to be uppercase
    CHARACTER_GIVEN_NAME_MD=$(echo "${CHARACTER_GIVEN_NAME[$ROLL_CHARACTER_GIVEN_NAME]^}")
    CHARACTER_LAST_NAME_MD=$(echo "${CHARACTER_LAST_NAME[$ROLL_CHARACTER_LAST_NAME]^}")
}

function rollBirthday() {
    # Generate birthday
    source ${CONFIG_FOLDER}/races/${CHARACTER_RACE}.conf
    CHARACTER_AGE=$(shuf -i ${RACE_AGE_ADULT_START}-${RACE_AGE_ADULT_END} -n 1)

    # Make a proper birthday out of the age
    MONTH=$(shuf -i 1-12 -n 1)
    case $MONTH in
        1|3|5|7|8|10|12)
            DAY=$(shuf -i 1-31 -n 1)
            ;;
        4|6|9|11)
            DAY=$(shuf -i 1-30 -n 1)
            ;;
        2)
            DAY=$(shuf -i 1-28 -n 1)
            ;;
    esac

    # Larvic Calendar style
    YEAR=$(($CURRENT_YEAR - $CHARACTER_AGE))
    if (( $CURRENT_YEAR < $CHARACTER_AGE ))
    then
        YEAR=$(echo $YEAR | sed 's/-//g')
        YEAR="$YEAR BLC"
    else
        YEAR="$YEAR LC"
    fi
    CHARACTER_BIRTHDAY="$DAY.$MONTH.$YEAR"
}

# For ySoccer
function rollSkinColor() {
    CHARACTER_SKIN_INDEX=$(shuf -i 1-${#SKINCOLORS[@]} -n 1)-1
    CHARACTER_SKINCOLOR=${SKINCOLORS[$CHARACTER_SKIN_INDEX]}
}

# For ySoccer
function rollHair() {
    CHARACTER_HAIRSTYLE_INDEX=$(shuf -i 1-${#HAIRSTYLES[@]} -n 1)-1
    CHARACTER_HAIRSTYLE=${HAIRSTYLES[$CHARACTER_HAIRSTYLE_INDEX]}

    CHARACTER_HAIRCOLOR_INDEX=$(shuf -i 1-${#HAIRCOLORS[@]} -n 1)-1
    CHARACTER_HAIRCOLOR=${HAIRCOLORS[$CHARACTER_HAIRCOLOR_INDEX]}
}

# For ySoccer
function rollPosition() {
  CHARACTER_POSITION_INDEX=$(shuf -i 1-${#POSITIONS_ARRAY[@]} -n 1)-1
  CHARACTER_POSITION_INGAME=${POSITIONS_ARRAY[$CHARACTER_POSITION_INDEX]}
  # Format the position without _ and uppercase first letters for website
  CHARACTER_POSITION_MD=$(echo "$CHARACTER_POSITION_INGAME" | tr '[:upper:]' '[:lower:]' | sed 's/_/ /g' | sed -e "s/\b\(.\)/\u\1/g")

}

# For ySoccer
function rollSkills() {
    SKILLNUM=()

    for (( i=0 ; i <= ${#SKILLS_ARRAY[@]} - 1 ; ++i ))
        do
            SKILLROLE=$(shuf -i 0-100 -n 1)

            if (( $SKILLROLE >= 1 && $SKILLROLE <= 5 ))
            then
                SKILLNUM[i]=0
            elif (( $SKILLROLE >= 6 && $SKILLROLE <= 15 ))
            then
                SKILLNUM[i]=1
            elif (( $SKILLROLE >= 16 && $SKILLROLE <= 30 ))
            then
                SKILLNUM[i]=2
            elif (( $SKILLROLE >= 31 && $SKILLROLE <= 65 ))
            then
                SKILLNUM[i]=3
            elif (( $SKILLROLE >= 66 && $SKILLROLE <= 75 ))
            then
                SKILLNUM[i]=4
            elif (( $SKILLROLE >= 76 && $SKILLROLE <= 85 ))
            then
                SKILLNUM[i]=5
            elif (( $SKILLROLE >= 86 && $SKILLROLE <= 95 ))
            then
                SKILLNUM[i]=6
            else
                SKILLNUM[i]=7
            fi
        done
}

# For ySoccer
function rollBestSkills() {
    BESTSKILL_1=$(shuf -i 1-${#SKILLS_ARRAY[@]} -n 1)-1
    BESTSKILL_2=$(shuf -i 1-${#SKILLS_ARRAY[@]} -n 1)-1
    BESTSKILL_3=$(shuf -i 1-${#SKILLS_ARRAY[@]} -n 1)-1

    while [ "${SKILLS_ARRAY[$BESTSKILL_2]}" == "${SKILLS_ARRAY[$BESTSKILL_1]}" ]
    do
        BESTSKILL_2=$(shuf -i 1-${#SKILLS_ARRAY[@]} -n 1)-1
    done
    while [ "${SKILLS_ARRAY[$BESTSKILL_3]}" == "${SKILLS_ARRAY[$BESTSKILL_1]}" ] || [ "${SKILLS_ARRAY[$BESTSKILL_3]}" == "${SKILLS_ARRAY[$BESTSKILL_2]}" ]
    do
        BESTSKILL_3=$(shuf -i 1-${#SKILLS_ARRAY[@]} -n 1)-1
    done
}

# For ySoccer
function generatePlayerJSON() {
    cat << EOF >> $UEFF_FOLDER/generated-teams/$TEAM_NAME_STRING/team.$TEAM_NAME_STRING.json
  {
    "name": "$CHARACTER_GIVEN_NAME_INGAME $CHARACTER_LAST_NAME_INGAME",
    "shirtName": "$CHARACTER_LAST_NAME_INGAME",
    "nationality": "$CHARACTER_NATIONALITY_INGAME",
    "role": "$CHARACTER_POSITION_INGAME",
    "number": "$CHARACTER_NUMBER",
    "skinColor": "$CHARACTER_SKINCOLOR",
    "hairColor": "$CHARACTER_HAIRCOLOR",
    "hairStyle": "$CHARACTER_HAIRSTYLE",
    "skills": {
      "passing": ${SKILLNUM[0]},
      "shooting": ${SKILLNUM[1]},
      "heading": ${SKILLNUM[2]},
      "tackling": ${SKILLNUM[3]},
      "control": ${SKILLNUM[4]},
      "speed": ${SKILLNUM[5]},
      "finishing": ${SKILLNUM[6]}
    },
    "bestSkills": [
      "${SKILLS_ARRAY[$BESTSKILL_1]}",
      "${SKILLS_ARRAY[$BESTSKILL_2]}",
      "${SKILLS_ARRAY[$BESTSKILL_3]}"
    ]
  },
EOF
}

# For Hugo website
function generatePlayerMD() {
    cat << EOF > ${UEFF_FOLDER}/generated-teams/$TEAM_NAME_STRING/players/${CHARACTER_GIVEN_NAME}_${CHARACTER_LAST_NAME}.md
---
author: "UEFF"
title: "$CHARACTER_GIVEN_NAME_MD $CHARACTER_LAST_NAME_MD"
date:
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | $CHARACTER_GIVEN_NAME_MD $CHARACTER_LAST_NAME_MD |
| Date of birth | $CHARACTER_BIRTHDAY |
| City of birth | [$CHARACTER_BIRTH_CITY_MD](/cities/$CHARACTER_BIRTH_CITY_WEB) |
| Nationality | ![$CHARACTER_NATIONALITY_MD](/images/flags/${CHARACTER_NATIONALITY}_small_md.png) [$CHARACTER_NATIONALITY_MD](/nations/$CHARACTER_NATIONALITY) |
| Race | [$CHARACTER_RACE_MD](/races/$CHARACTER_RACE) |
| Position | $CHARACTER_POSITION_MD |
| Current club | [$TEAM_NAME_SHORT](/teams/$TEAM_NAME_STRING) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [$TEAM_NAME_SHORT](/teams/$TEAM_NAME_STRING) | $CURRENT_YEAR | | |

## Honours

EOF

    cat << EOF >> ${UEFF_FOLDER}/generated-teams/$TEAM_NAME_STRING/index.md
| $CHARACTER_NUMBER |    $CHARACTER_POSITION_MD    | ![$CHARACTER_NATIONALITY_MD](/images/flags/${CHARACTER_NATIONALITY}_small_md.png) | [$CHARACTER_GIVEN_NAME_MD $CHARACTER_LAST_NAME_MD](/characters/${CHARACTER_GIVEN_NAME}_${CHARACTER_LAST_NAME}) | [$CHARACTER_RACE_MD](/races/$CHARACTER_RACE)     | $CHARACTER_BIRTHDAY |
EOF
}

function generateManager() {
    # Generate Manager
    rollRace
    rollName
    rollBirthday
    rollNationality
    TEAM_MANAGER_GIVEN_NAME=$CHARACTER_GIVEN_NAME_MD
    TEAM_MANAGER_LAST_NAME=$CHARACTER_LAST_NAME_MD
    TEAM_MANAGER_GIVEN_NAME_INGAME=$(echo $CHARACTER_GIVEN_NAME_MD | tr '[:lower:]' '[:upper:]')
    TEAM_MANAGER_LAST_NAME_INGAME=$(echo $CHARACTER_LAST_NAME_MD | tr '[:lower:]' '[:upper:]')
    TEAM_MANAGER_NATIONALITY=$CHARACTER_NATIONALITY
    TEAM_MANAGER_NATIONALITY_MD=$CHARACTER_NATIONALITY_MD

    # Generate markdown file for the Hugo website
    cat << EOF > ${UEFF_FOLDER}/generated-teams/$TEAM_NAME_STRING/manager/${CHARACTER_GIVEN_NAME}_${CHARACTER_LAST_NAME}.md
---
author: "UEFF"
title: "$CHARACTER_GIVEN_NAME_MD $CHARACTER_LAST_NAME_MD"
date:
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | $CHARACTER_GIVEN_NAME_MD $CHARACTER_LAST_NAME_MD |
| Date of birth | $CHARACTER_BIRTHDAY |
| City of birth | [$CHARACTER_BIRTH_CITY_MD](/cities/$CHARACTER_BIRTH_CITY_WEB) |
| Nationality | ![$CHARACTER_NATIONALITY_MD](/images/flags/${CHARACTER_NATIONALITY}_small_md.png) [$CHARACTER_NATIONALITY_MD](/nations/$CHARACTER_NATIONALITY) |
| Race | [$CHARACTER_RACE_MD](/races/$CHARACTER_RACE) |
| Position | Coach |
| Current club | [$TEAM_NAME_SHORT](/teams/$TEAM_NAME_STRING) |

## Club career

| Club | Season | League position |
|-|-|-|
| [$TEAM_NAME_SHORT](/teams/$TEAM_NAME_STRING) | $CURRENT_YEAR | | |

## Honours

EOF
}

function main() {
    if [[ $GEN_MANAGER = 1 ]];then
        generateManager
        export TEAM_MANAGER_GIVEN_NAME TEAM_MANAGER_LAST_NAME TEAM_MANAGER_GIVEN_NAME_INGAME TEAM_MANAGER_LAST_NAME_INGAME TEAM_MANAGER_NATIONALITY TEAM_MANAGER_NATIONALITY_MD
        GEN_MANAGER=0
    else
        echo "Generate Manager ... SKIP"
    fi

    if [[ $GEN_CHARACTERS = 1 ]];then
        CHARACTERS_GENERATED=0
        CHARACTER_NUMBER=1

        for (( POSITION_COUNTER=0 ; POSITION_COUNTER <= ${#REQUIRED_POS[@]} ; ++POSITION_COUNTER ))
        do
            while [[ ! -z ${REQUIRED_NUM_POS[$POSITION_COUNTER]} && ${REQUIRED_NUM_POS[POSITION_COUNTER]} -gt $CHARACTERS_GENERATED ]]
            do
                rollRace
                rollName
                rollBirthday
                rollNationality
                rollSkinColor
                rollHair
                rollPosition
                rollSkills
                rollBestSkills
                if [ "$CHARACTER_POSITION_INGAME" = "${REQUIRED_POS[POSITION_COUNTER]}" ];then
                    generatePlayerJSON
                    generatePlayerMD
                    CHARACTERS_GENERATED=$(($CHARACTERS_GENERATED+1))
                    CHARACTER_NUMBER=$(($CHARACTER_NUMBER+1))
                fi
            done
            CHARACTERS_GENERATED=0
        done
    else
        echo "Generate Players ... SKIP"
    fi
}

main
