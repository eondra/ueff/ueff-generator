#!/bin/bash

source ${CONFIG_FOLDER}/general.conf

function cliOutput() {
    echo "$CLIMESSAGE"
}

function getUserInput() {
    read -p "Enter the teams full name (case sensitive): " TEAM_NAME_FULL
    read -p "Enter the teams short name (case sensitive): " TEAM_NAME_SHORT
    read -p "Enter the teams nation (case sensitive): " TEAM_NATION
    read -p "Enter the teams city (case sensitive): " TEAM_CITY
    read -p "Enter the teams stadium name (case sensitive): " TEAM_STADIUM_NAME
    read -p "Enter the teams founding date (Format: dd/mm/yy LC): " TEAM_FOUND_DATE
}

function formatStrings() {
    # TEAM_NAME_STRING is for our directory/file name. NO SPACES! and lower case
    TEAM_NAME_STRING=$(echo "$TEAM_NAME_SHORT" | tr '[:upper:]' '[:lower:]' | sed 's/ //g')
    # All In-game strings need to be upper case
    TEAM_NAME_INGAME=$(echo "$TEAM_NAME_SHORT" | tr '[:lower:]' '[:upper:]')
    TEAM_STADIUM_INGAME=$(echo "$TEAM_STADIUM_NAME" | tr '[:lower:]' '[:upper:]')
    TEAM_CITY_INGAME=$(echo "$TEAM_CITY" | tr '[:lower:]' '[:upper:]')
    TEAM_NATION_CODE=$(echo "$TEAM_NATION" | cut -c1-3 | tr '[:lower:]' '[:upper:]')
    TEAM_NATION_LC=$(echo "$TEAM_NATION" | tr '[:upper:]' '[:lower:]')
}

function createFolders() {
    CLIMESSAGE="Generating folders"
    cliOutput
    mkdir -p ${UEFF_FOLDER}/generated-teams/${TEAM_NAME_STRING}/players
    mkdir -p ${UEFF_FOLDER}/generated-teams/${TEAM_NAME_STRING}/manager
}

function generateManager() {
    CLIMESSAGE="Generating manager"
    cliOutput
    export TEAM_NATION_LC TEAM_NAME_SHORT TEAM_NAME_STRING GEN_MANAGER=1
    . ${TABLETOP_CHARACTER_SCRIPT}
    GEN_MANAGER=0
}

# For Hugo website
function generateTeamMD() {
    CLIMESSAGE="Generating markdown file"
    cliOutput
    # Generate markdown file of the team for the website
    cat << EOF > ${UEFF_FOLDER}/generated-teams/${TEAM_NAME_STRING}/index.md
---
author: "UEFF"
title: "$TEAM_NAME_SHORT"
date:
draft: false
type: "showcase"
---

## General info

| | |
|-|-|
| Name | $TEAM_NAME_FULL |
| Nation | [$TEAM_NATION](/nations/${TEAM_NATION_LC}) |
| City | $TEAM_CITY |
| Founded | $TEAM_FOUND_DATE |
| Field | $TEAM_STADIUM_NAME |
| Manager | ![$TEAM_MANAGER_NATIONALITY_MD](/images/flags/${TEAM_MANAGER_NATIONALITY}_small_md.png) | [$TEAM_MANAGER_GIVEN_NAME $TEAM_MANAGER_LAST_NAME](/characters/${TEAM_MANAGER_GIVEN_NAME}_${TEAM_MANAGER_LAST_NAME})


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
EOF
}

function generatePlayers() {
    CLIMESSAGE="Generating characters"
    cliOutput
    export TEAM_NATION_LC TEAM_NAME_SHORT TEAM_NAME_STRING GEN_CHARACTERS=1
    . ${TABLETOP_CHARACTER_SCRIPT}
    GEN_CHARACTERS=0
}

# For Hugo website
function finishTeamMD() {
    CLIMESSAGE="Finishing markdown file"
    cliOutput
    cat << EOF >> ${UEFF_FOLDER}/generated-teams/$TEAM_NAME_STRING/index.md

## Honours

EOF
}

function finalOutput() {
    CLIMESSAGE="Your team has been saved at ${UEFF_FOLDER}/generated-teams/${TEAM_NAME_STRING}"
    cliOutput
}

function main() {
    getUserInput
    formatStrings
    createFolders
    generateManager
    generateTeamMD
    generatePlayers
    finishTeamMD
    finalOutput
}

main
