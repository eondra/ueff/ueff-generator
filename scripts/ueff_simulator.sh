#!/bin/bash

source ${CONFIG_FOLDER}/general.conf

TURN_COUNTER=1
HOME_SCORE=0
AWAY_SCORE=0
KICKOFF_AFTER_SCORE=0

# For statistics
HOME_COMPLETED_PASSES=0
AWAY_COMPLETED_PASSES=0

HOME_SHOTS=0
AWAY_SHOTS=0

HOME_SHOTS_ON_GOAL=0
AWAY_SHOTS_ON_GOAL=0

userInput() {
    read -p "Competition: (Friendly/Competitive): " GAME_TYPE
    read -p "Home Team: " HOME_TEAM
    read -p "AWay Team: " AWAY_TEAM
}

genAttendance() {
    ATTENDANCE=$(shuf -i 100-2000 -n 1)
}

rollKickoff() {
    if [ $KICKOFF_AFTER_SCORE -eq 0 ]; then
        KICKOFF_ROLL=$(shuf -i 1-20 -n 1)
        if [ $KICKOFF_ROLL -le 10 ]; then
            KICKOFF_TEAM="$HOME_TEAM"
            ORB_POSSESSION="$HOME_TEAM"
            echo "Kickoff goes to: $KICKOFF_TEAM"
        else
            KICKOFF_TEAM="$AWAY_TEAM"
            ORB_POSSESSION="$AWAY_TEAM"
            echo "Kickoff goes to: $KICKOFF_TEAM"
        fi
    fi
    rollKickoffDirection
}

rollKickoffDirection() {
    KICKOFF_DIRECTION_ROLL=$(shuf -i 1-20 -n 1)
    if [ $KICKOFF_DIRECTION_ROLL -le 15 ]; then
        ORB_POSITION="DEF"
        echo "Kickoff into the Defense"
    else
        echo "Kickoff into the Midfield"
        ORB_POSITION="MID"
    fi
}

checkPassDirection() {
    PASS_DIRECTION_ROLL=$(shuf -i 1-20 -n 1)
    if [ $ORB_POSITION = "DEF" ]; then
        if [ $PASS_DIRECTION_ROLL -le 15 ]; then
            ORB_POSITION="MID"
            echo "Pass into the Midfield"
            rollPass
        else
            echo "Pass stay in the Defense"
            rollPass
        fi
    elif [ $ORB_POSITION = "MID" ]; then
        if [ $PASS_DIRECTION_ROLL -gt 17 ]; then
            ORB_POSITION="DEF"
            echo "Pass into the Defense"
            rollPass
        elif [ $PASS_DIRECTION_ROLL -le 6 ]; then
            ORB_POSITION="ATT"
            echo "Pass into the Attack"
            rollPass
        else
            echo "Pass stay in the Midfield"
            rollPass
        fi
    elif [ $ORB_POSITION = "ATT" ]; then
        if [ $PASS_DIRECTION_ROLL -le 15 ]; then
            echo "$ORB_POSSESSION is taking a shot!"
            rollShot
        else
            ORB_POSITION="MID"
            echo "Pass into the Midfield"
            rollPass
        fi
    fi
}

rollPass() {
    PASS_ROLL=$(shuf -i 1-20 -n 1)
    if [ $PASS_ROLL -le 15 ]; then
        echo "Pass complete!"
        if [ "$ORB_POSSESSION" = "$HOME_TEAM" ]; then
            HOME_COMPLETED_PASSES=$(($HOME_COMPLETED_PASSES+1))
        else
            AWAY_COMPLETED_PASSES=$(($AWAY_COMPLETED_PASSES+1))
        fi
        checkPassChallenge
    else
        echo "Pass incomplete!"
        checkOrbChallenge
    fi
}

checkPassChallenge() {
    CHALLENGE_ROLL=$(shuf -i 1-20 -n 1)
    if [ $CHALLENGE_ROLL -le 15 ]; then
        checkOrbChallenge
    else
        echo "Pass unchallenged"
        if [ "$ORB_POSSESSION" = "$HOME_TEAM" ]; then
            HOME_COMPLETED_PASSES=$(($HOME_COMPLETED_PASSES+1))
        else
            AWAY_COMPLETED_PASSES=$(($AWAY_COMPLETED_PASSES+1))
        fi
    fi
}

checkOrbChallenge() {
    HOME_TEAM_ROLL=0
    AWAY_TEAM_ROLL=0
    while [ $HOME_TEAM_ROLL -eq $AWAY_TEAM_ROLL ]
    do
        HOME_TEAM_ROLL=$(shuf -i 1-20 -n 1)
        AWAY_TEAM_ROLL=$(shuf -i 1-20 -n 1)
        echo "Challenge Roll: $HOME_TEAM_ROLL : $AWAY_TEAM_ROLL"
    done
    if [ $HOME_TEAM_ROLL -gt $AWAY_TEAM_ROLL ] && [ "$ORB_POSSESSION" = "$HOME_TEAM" ]; then
        echo "$HOME_TEAM stay in possession despite challenge."
    elif
        [ $HOME_TEAM_ROLL -gt $AWAY_TEAM_ROLL ] && [ "$ORB_POSSESSION" = "$AWAY_TEAM" ]; then
        echo "$HOME_TEAM wins the challenge and is now in possession"
        changeOrbPosition
        changeOrbPossession
    elif [ $AWAY_TEAM_ROLL -gt $HOME_TEAM_ROLL ] && [ "$ORB_POSSESSION" = "$AWAY_TEAM" ]; then
        echo "$AWAY_TEAM stay in possession despite challenge."
    elif [ $AWAY_TEAM_ROLL -gt $HOME_TEAM_ROLL ] && [ "$ORB_POSSESSION" = "$HOME_TEAM" ]; then
        echo "$AWAY_TEAM wins the challenge and is now in possession"
        changeOrbPosition
        changeOrbPossession
    fi
}

changeOrbPosition() {
    if [ $ORB_POSITION = "ATT" ]; then
        ORB_POSITION="DEF"
    elif [ $ORB_POSITION = "DEF" ]; then
        ORB_POSITION="ATT"
    fi
}

changeOrbPossession() {
    if [ "$ORB_POSSESSION" = "$HOME_TEAM" ]; then
        ORB_POSSESSION="$AWAY_TEAM"
    else
        ORB_POSSESSION="$HOME_TEAM"
    fi
    TURN_COUNTER=$(($TURN_COUNTER+1))
}

rollShot() {
    if [ "$ORB_POSSESSION" = "$HOME_TEAM" ]; then
            HOME_SHOTS=$(($HOME_SHOTS+1))
    else
            AWAY_SHOTS=$(($AWAY_SHOTS+1))
    fi
    SHOT_ROLL=$(shuf -i 1-20 -n 1)
    if [ $SHOT_ROLL -le 13 ]; then
        if [ "$ORB_POSSESSION" = "$HOME_TEAM" ]; then
            HOME_SHOTS_ON_GOAL=$(($HOME_SHOTS_ON_GOAL+1))
        else
            AWAY_SHOTS_ON_GOAL=$(($AWAY_SHOTS_ON_GOAL+1))
        fi
        HOME_ROLL=$(shuf -i 1-20 -n 1)
        AWAY_ROLL=$(shuf -i 1-20 -n 1)
        echo "Shot rolls: $HOME_ROLL - $AWAY_ROLL"
        if [ $HOME_ROLL -gt $AWAY_ROLL ] && [ "$ORB_POSSESSION" = "$HOME_TEAM" ]; then
            echo "$HOME_TEAM scored!"
            HOME_SCORE=$(($HOME_SCORE+1))
            changeOrbPossession
            KICKOFF_AFTER_SCORE=1
            rollKickoff
        elif [ $AWAY_ROLL -gt $HOME_ROLL ] && [ "$ORB_POSSESSION" = "$AWAY_TEAM" ]; then
            echo "$AWAY_TEAM scored!"
            AWAY_SCORE=$(($AWAY_SCORE+1))
            changeOrbPossession
            KICKOFF_AFTER_SCORE=1
            rollKickoff
        else
            echo "Shot was denied!"
            changeOrbPosition
            changeOrbPossession
        fi
    else
        echo "Shot goes wide"
        changeOrbPosition
        changeOrbPossession
    fi
}

prepareGame() {
    echo "Game Type: $GAME_TYPE" 
    genAttendance
}

simulateGame() {
    rollKickoff
    while [ $TURN_COUNTER -le 90 ]
    do
        echo "Turn: $TURN_COUNTER"
        echo "Orb position: $ORB_POSITION"
        echo "Orb possession: $ORB_POSSESSION"
        checkPassDirection
    done
    echo "Game finished"
}

genStatistics() {
    echo "Attendance: $ATTENDANCE"
    echo "$HOME_TEAM - $AWAY_TEAM $HOME_SCORE:$AWAY_SCORE"
    echo "Possession: $HOME_COMPLETED_PASSES:$AWAY_COMPLETED_PASSES"
    echo "Shots: $HOME_SHOTS:$AWAY_SHOTS"
    echo "Shots On Goal: $HOME_SHOTS_ON_GOAL:$AWAY_SHOTS_ON_GOAL"
}

userInput
prepareGame
simulateGame
genStatistics
