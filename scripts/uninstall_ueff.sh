#!/bin/bash

read -p "Do you really want to uninstall UEFF and all created data? (y/N) " UNINSTALL_YN

if [ "$UNINSTALL_YN" == Y ] || [ "$UNINSTALL_YN" == y ];then
    source ${CONFIG_FOLDER}/general.conf
    echo "Uninstalling UEFF..."
    rm -Rf ${UEFF_FOLDER}
    rm -Rf ${CONFIG_FOLDER}
    sudo rm -Rf ${SCRIPT_FOLDER}
    sudo rm /usr/bin/ueff
    echo "DONE"
    export UEFF_QUIT=1
else
    echo "Canceled"
    exit 0
fi
